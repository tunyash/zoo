#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0+

import os
import itertools
import sys
import enum

# Input
import yaml

#####
# https://gist.github.com/pypt/94d747fe5180851196eb
# Fixes https://github.com/yaml/pyyaml/issues/41
class UniqueKeyLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = []
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            if key in mapping:
                raise ValueError("Duplicate key " + key)
            mapping.append(key)
        return super().construct_mapping(node, deep)
#####

#####
# Hack to avoid parsing Zbl ids as Integers
# Disables special parsing of any scalar starting with 0
del UniqueKeyLoader.yaml_implicit_resolvers['0']
#####

#####
# https://stackoverflow.com/questions/13319067/parsing-yaml-return-with-line-number
class LineNoLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = super().construct_mapping(node, deep)
        mapping["lineno"] = node.start_mark.line
        return mapping
#####

# Processing
import networkx as nx

# Output
import re
import jinja2
from networkx.drawing.nx_agraph import to_agraph
from pylatexenc.latex2text import LatexNodes2Text

# DBLP
import urllib.request
import bs4

# ZBMath
import bibtexparser

CONFIG_OUTPUT=""
CONFIG_QP_SIM=True

# Vertices are proof systems (duplicated) and formulas.
# Edge from A to B if A simulates B
# Edge from A' to B' if A simulates B
# Edge from A to F if A can prove F efficiently
# Edge from F to A' if A cannot prove F efficiently
Gsep = nx.DiGraph()

with open('zoo.yaml','r') as f:
    data = yaml.load(f, UniqueKeyLoader)
    f.seek(0)
    datalines = yaml.load(f, LineNoLoader)

#print(yaml.dump(data),file=sys.stderr)

proofsystems = data["proofsystems"]
for ps,v in proofsystems.items():
    Gsep.add_node(ps)
    Gsep.add_node(ps+"_")
    v['unicodename'] = LatexNodes2Text().latex_to_text(v['longname'])

formulas = data["formulas"]
for f in formulas:
    Gsep.add_node(f)

def dblp(v):
    key=v["dblp"]
    url=f"http://dblp.uni-trier.de/rec/bibtex/{key}.xml"
    v["url"]=f"http://dblp.uni-trier.de/rec/{key}.html"
    localkey=key.translate(str.maketrans('/','_'))
    filename=f"cache/{localkey}.xml"
    if not os.path.isfile(filename):
        urllib.request.urlretrieve(url, filename)
    with open(filename) as f:
        bib = bs4.BeautifulSoup(f, "html.parser")
        v["title"] = bib.title.string
    #print(v,file=sys.stderr)
    return v

def zbmath(v):
    key=v["zbl"]
    url=f"http://zbmath.org/bibtex/{key}.bib"
    v["url"]=f"http://zbmath.org/{key}"
    localkey=key
    filename=f"cache/{localkey}.bib"
    if not os.path.isfile(filename):
        urllib.request.urlretrieve(url, filename)
    with open(filename) as f:
        parser = bibtexparser.bparser.BibTexParser()
        parser.customization = bibtexparser.customization.convert_to_unicode
        bib = bibtexparser.load(f,parser=parser).entries[0]
        v["title"] = bib['title']
    #print(v,file=sys.stderr)
    return v

papers = data["papers"]
for p,v in papers.items():
    if "dblp" in v:
        papers[p]=dblp(v)
    elif "zbl" in v:
        papers[p]=zbmath(v)

overheadvalues = {
    "none": 0,
    "poly": 0,
    "quasipoly": 1,
}
sizevalues = {
    "poly": 0,
    "quasipoly": 1,
    "exp": 1000,
}

def checkps(ps):
    if ps not in proofsystems:
        raise ValueError(f"{ps} is not a proof system id, check for typos or add a new proof system")

def checkf(f):
    if f not in formulas:
        raise ValueError(f"{f} is not a formula id, check for typos or add a new formula")

def checkpaper(p):
    if p not in papers:
        raise ValueError(f"{p} is not a paper id, check for typos or add a new paper")

def ensurelist(l):
    if isinstance(l, list):
        return l
    elif l is None:
        return []
    else:
        return [l]

def listorscalar(m, name):
    if name in m:
        l=m[name]
        return ensurelist(m[name])
    return []

def transposecall(f):
    return lambda x,y : f(y,x)

for ps,z in proofsystems.items():
    for p in listorscalar(z,'source'):
        checkpaper(p)

subsystems = {ps : set() for ps in proofsystems}
for weak,z in proofsystems.items():
    for strong in listorscalar(z,'subsystemof')+listorscalar(z,'subsystem of'):
        checkps(strong)
        checkps(weak)
        subsystems[strong].add(weak)
        Gsep.add_edge(strong, weak, weight=0)
        Gsep.add_edge(strong+"_", weak+"_", weight=0)

for weak,z in formulas.items():
    for strong in listorscalar(z,'extends'):
        checkf(strong)
        checkf(weak)
        Gsep.add_edge(strong, weak, weight=0)

simulations = data["simulations"]
for strong,z in simulations.items():
    for weak,s in z.items():
        checkps(strong)
        checkps(weak)
        overhead = s.get("overhead", "poly")
        v = overheadvalues[overhead]
        Gsep.add_edge(strong, weak, weight=v)
        Gsep.add_edge(strong+"_", weak+"_", weight=v)

upperbounds = data["upperbounds"]
for ps,z in upperbounds.items():
    if isinstance(z,str):
        upperbounds[ps] = z = {z: {}}
    for f,ub in z.items():
        checkps(ps)
        checkf(f)
        if ub is None:
            upperbounds[ps][f] = ub = {}
        size = ub.get("size", "poly")
        v = sizevalues[size]
        Gsep.add_edge(ps, f, weight=v)

lowerbounds = data["lowerbounds"]
for ps,z in lowerbounds.items():
    if isinstance(z,str):
        lowerbounds[ps] = z = {z: {}}
    for f,lb in z.items():
        checkps(ps)
        checkf(f)
        if lb is None:
            lowerbounds[ps][f] = lb = {}
        size = lb["size"]
        v = sizevalues[size]
        Gsep.add_edge(f, ps+"_", weight=-v)

reach = dict(nx.all_pairs_bellman_ford_path_length(Gsep))
reachpath = dict(nx.all_pairs_bellman_ford_path(Gsep))
#print(reach, file=sys.stderr)
notpolybounded = set()
for f in formulas:
    notpolybounded.update(s[:-1] for s in reach[f] if s[-1]=='_')
nolowerbounds = set(proofsystems) - notpolybounded

Relation = enum.Enum('Relation', 'EQUIV STRONGER WEAKER INCOMP SIMS SIMD NSIMS NSIMD UNREL')
rel_to_string = {
    Relation.EQUIV: "equivalent",
    Relation.STRONGER: "stronger than",
    Relation.WEAKER: "weaker than",
    Relation.INCOMP: "incomparable wrt",
    Relation.SIMS: "simulates",
    Relation.SIMD: "simulated by",
    Relation.NSIMS: "does not simulate",
    Relation.NSIMD: "not simulated by",
    Relation.UNREL: "[missing?]",
}

def check_simulation(ps1, ps2):
    if CONFIG_QP_SIM:
        return ps2 in reach[ps1]
    else:
        return (reach[ps1].get(ps2,-1))==0

def check_separation(ps1, ps2):
    ps2 = ps2+"_"
    return (reach[ps1].get(ps2,0))<0


def check_relations(ps1, ps2):
    sim12 = check_simulation(ps1, ps2)
    sim21 = check_simulation(ps2, ps1)
    sep12 = check_separation(ps1, ps2)
    sep21 = check_separation(ps2, ps1)
    #print(ps1,ps2,sim12,sim21,sep12,sep21, file=sys.stderr)
    return sim12, sim21, sep12, sep21

def relation_id(ps1, ps2):
    sim12, sim21, sep12, sep21 = check_relations(ps1, ps2)
    if sim12 and sim21 : return Relation.EQUIV
    elif sim12 and sep12 : return Relation.STRONGER
    elif sim21 and sep21 : return Relation.WEAKER
    elif sep12 and sep21 : return Relation.INCOMP
    elif sim12 : return Relation.SIMS
    elif sim21 : return Relation.SIMD
    elif sep21 : return Relation.NSIMS
    elif sep12 : return Relation.NSIMD
    else: return Relation.UNREL

def relation_natural(ps1, ps2):
    sim12, sim21, sep12, sep21 = check_relations(ps1, ps2)
    name1 = proofsystems[ps1]['unicodename']
    name2 = proofsystems[ps2]['unicodename']
    #print(ps1,ps2,sim12,sim21,sep12,sep21, file=sys.stderr)
    if sim12 and sim21 :
        return f"{name1} and {name2} equivalent"
    elif sim12 and sep12 :
        return f"{name1} stronger than {name2}"
    elif sim21 and sep21 :
        return f"{name2} stronger than {name1}"
    elif sep12 and sep21 :
        return f"{name1} and {name2} incomparable"
    elif sim12 :
        return f"{name1} simulates {name2}"
    elif sim21 :
        return f"{name2} simulates {name1}"
    elif sep12 :
        return f"{name2} does not simulate {name1}"
    elif sep21 :
        return f"{name1} does not simulate {name2}"
    else:
        return f"{name1} and {name2} unrelated"

def complexity_id(ps, f):
    if f in reach[ps]:
        #print(f"{ps} {f} {reach[ps][f]}")
        if reach[ps][f]==0 : return "polynomial"
        if reach[ps][f]>0 : return "at most quasipolynomial"
    if ps+"_" in reach[f]:
        #print(f"{ps} {f} {reach[f][ps+'_']}")
        if reach[f][ps+"_"]<-900 : return "exponential"
        if reach[f][ps+"_"]<0 : return "superpolynomial"
    return "[missing?]"

def format_as_path(l):
    return " → ".join(x.strip("_") for x in l)

def get_sources(d):
    sources = listorscalar(d, "source")
    for s in sources:
        checkpaper(s)
    if (sources):
        return sources
    else:
        return "[citation needed]"

def complexity_source(ps, f):
    if ps in lowerbounds:
        if f in lowerbounds[ps]:
            return get_sources(lowerbounds[ps][f])
    if ps in upperbounds:
        if f in upperbounds[ps]:
            return get_sources(upperbounds[ps][f])
    if f in reachpath[ps]:
        return format_as_path(reachpath[ps][f])
    if ps+"_" in reachpath[f]:
        return format_as_path(reachpath[f][ps+"_"])
    return []

def relation_source_ub(ps1, ps2):
    if ps1 in simulations:
        if ps2 in simulations[ps1]:
            return get_sources(simulations[ps1][ps2])
    if ps2 in simulations:
        if ps1 in simulations[ps2]:
            return get_sources(simulations[ps2][ps1])
    if ps2 in subsystems[ps1] or ps1 in subsystems[ps2]:
        return "[subsystem]"
    if ps2 in reachpath[ps1]:
        return format_as_path(reachpath[ps1][ps2])
    if ps1 in reachpath[ps2]:
        return format_as_path(reachpath[ps2][ps1])
    return None

def relation_source_lb(ps1, ps2):
    sources = []
    if ps2+"_" in reachpath[ps1]:
        sources.append(format_as_path(reachpath[ps1][ps2+"_"]))
    if ps1+"_" in reachpath[ps2]:
        sources.append(format_as_path(reachpath[ps2][ps1+"_"]))
    return sources

def relation_source(ps1, ps2):
    ub = ensurelist(relation_source_ub(ps1, ps2))
    lb = ensurelist(relation_source_lb(ps1, ps2))
    return ub + lb

def complexity(ps, f):
    ret = {"ps": ps,
            "f": f,
            "id": complexity_id(ps, f)
           }
    source = complexity_source(ps, f)
    if source:
        ret["source"]=ensurelist(source)
    return ret

def complexity_natural(ps, f, cplex):
    return f"The complexity of {f} in {ps} is {cplex}"

def relation(ps1, ps2):
    ret = {"ps1": ps1,
           "ps2": ps2,
           "id": relation_id(ps1, ps2),
           "str": rel_to_string[relation_id(ps1, ps2)]
           }
    source = relation_source(ps1, ps2)
    if source:
        ret["source"]=ensurelist(source)
    return ret


#for ps1, ps2 in itertools.combinations(proofsystems,2):
#    print(relation(ps1, ps2))

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
# https://stackoverflow.com/questions/12791216/how-do-i-use-regular-expressions-in-jinja2
def strip_svg_header(s):
    return re.sub('<[?]xml.*?[?]>|<!DOCTYPE.*?>', '', s, flags=re.DOTALL)

def visible_dots(s):
    return re.sub('stroke-dasharray="1,5"', 'stroke-dasharray="1,1"', s)

templateEnv.filters['strip_svg_header'] = strip_svg_header
templateEnv.filters['visible_dots'] = visible_dots
indexTemplate = templateEnv.get_template("index.html")
proofsystemTemplate = templateEnv.get_template("proofsystem.html")
formulaTemplate = templateEnv.get_template("formula.html")

def all_about_ps(ps1):
    relations = [relation(ps1, ps2) for ps2 in proofsystems if ps1 != ps2]
    complexities = [complexity(ps1, f) for f in formulas]
    with open(f"build/{CONFIG_OUTPUT}{ps1}.html", "w") as f:
        print(proofsystemTemplate.render(
            ps1=ps1,
            relations=relations,
            complexities=complexities,
            proofsystems=proofsystems,
            formulas=formulas,
            papers=papers), file=f)

def all_about_formula(ff):
    complexities = [complexity(ps, ff) for ps in proofsystems]
    with open(f"build/{CONFIG_OUTPUT}{ff}.html", "w") as f:
        print(formulaTemplate.render(
            ff=ff,
            complexities=complexities,
            proofsystems=proofsystems,
            formulas=formulas,
            papers=papers), file=f)

def index():
    with open(f"build/{CONFIG_OUTPUT}index.html", "w") as f:
        print(indexTemplate.render(
            proofsystems=proofsystems,
            formulas=formulas,
            papers=papers,
            CONFIG_OUTPUT=CONFIG_OUTPUT), file=f)

def make_pages():
    for ps1 in proofsystems:
        all_about_ps(ps1)
    for ff in formulas:
        all_about_formula(ff)
    index()

def raw_plot():
    H = to_agraph(Gsep)

    H.graph_attr["forcelabels"]=True
    H.graph_attr["newrank"]=True
    H.graph_attr["splines"]="polyline"

    H.write("zoo-raw.dot")
    os.system("dot -T pdf zoo-raw.dot -o build/zoo-raw.pdf")

def immediate_relations_graph():
    Grel = Gsep.copy()
    Grel.remove_nodes_from(formulas)
    Gsep_allformulas = Gsep.copy()
    for ps in proofsystems:
        for f in formulas:
            if f in reach[ps]:
                Gsep_allformulas.add_edge(ps,f)
    directreach = dict(nx.all_pairs_shortest_path_length(Gsep_allformulas, cutoff=2))
    for ps1, ps2 in itertools.product(proofsystems, repeat=2):
        if ps2+"_" in directreach[ps1]:
            Grel.add_edge(ps1,ps2+"_")
    if not CONFIG_QP_SIM:
        to_be_removed = []
        to_be_added = []
        for ps1, ps2, w in Grel.edges(data="weight"):
            if w is None or w<=0: continue
            to_be_removed.append((ps1,ps2))
            for ps3 in proofsystems:
                if ps1+"_" in directreach[ps3]:
                    to_be_added.append((ps3,ps2+"_"))
        Grel.remove_edges_from(to_be_removed)
        Grel.add_edges_from(to_be_added)
    return Grel

def minimal_relations_graph(Grel):
    Gcond = nx.algorithms.components.condensation(Grel)
    scc = Gcond.graph['mapping']
    classes = {}
    for k,v in scc.items():
        classes.setdefault(v, []).append(k)
    Gtrans = nx.algorithms.dag.transitive_reduction(Gcond)
    return scc,classes,Gtrans

def canonical_representatives(classes):
    canonical = {}
    for k,comp in classes.items():
        def by_lineno(v):
            if v[-1]=='_': v=v[:-1]
            return datalines["proofsystems"][v]['lineno']
        comp = sorted(comp, key=by_lineno)
        classes[k]=comp
        for i,ps in enumerate(comp):
            canonical[ps]=(i==0)
    return canonical

def find_natural_endpoints(Grel,classes,a,b,sa,sb):
    if Grel.has_edge(a,b) or Grel.has_edge(b,a):
        return (a,b)
    for u,v in itertools.product(classes[sa],classes[sb]):
        if Grel.has_edge(u,v) or Grel.has_edge(v,u):
                return (u,v) 
    return (a,b)

def clean_plot(minimal=False):
    Gclean = nx.DiGraph()
    for ps in proofsystems:
        Gclean.add_node(ps,shape="ellipse",margin="0",style="filled",fillcolor="#e7e7e7",peripheries="0")

    def getweight(ps1, ps2):
        return 100 if ps1 in subsystems[ps2] or ps2 in subsystems[ps1] else 1

    def equivalence(ps1, ps2):
        Gclean.add_edge(ps1,ps2,dir="both",constraint=False,color="#222288",weight=getweight(ps1,ps2),tooltip=relation_natural(ps1,ps2))

    def simsep(ps1, ps2):
        Gclean.add_edge(ps1,ps2,style="dashed",color="#882288",weight=getweight(ps1,ps2),tooltip=relation_natural(ps1,ps2))

    def osim(ps1, ps2):
        Gclean.add_edge(ps1,ps2,style="solid",color="#222222",weight=getweight(ps1,ps2),tooltip=relation_natural(ps1,ps2))

    def osep(ps1, ps2):
        Gclean.add_edge(ps1,ps2,constraint=False,style="dotted",color="#882222",tooltip=relation_natural(ps1,ps2))

    def sepsep(ps1, ps2):
        Gclean.add_edge(ps1,ps2,constraint=False,dir="both",style="dotted",color="#aa6622",tooltip=relation_natural(ps1,ps2))

    rel_to_edge = {
        Relation.EQUIV: equivalence,
        Relation.STRONGER: simsep,
        Relation.WEAKER: transposecall(simsep),
        Relation.INCOMP: sepsep,
        Relation.SIMS: osim,
        Relation.SIMD: transposecall(osim),
        Relation.NSIMS: transposecall(osep),
        Relation.NSIMD: osep,
        Relation.UNREL: lambda ps1, ps2: None,
    }

    Grel = immediate_relations_graph()
    scc,classes,Gtrans = minimal_relations_graph(Grel)
    canonical = canonical_representatives(classes)

    for ps1, ps2 in itertools.combinations(proofsystems, 2):
        scc1=scc[ps1]
        scc2=scc[ps2]
        if scc1==scc2:
# begin hirsch bugfix 1: find_natural_endpoints leaves SCC unconnected
#           if canonical[ps1] or canonical[ps2]:   
#               a,b = find_natural_endpoints(Grel,classes,ps1,ps2,scc1,scc2)
#               equivalence(a,b)
# end hirsch bugfix 1
            if Grel.has_edge(ps1,ps2):
                equivalence(ps1,ps2)
        if not canonical[ps1] or not canonical[ps2]:
            continue
        scc1_=scc[ps1+"_"]
        scc2_=scc[ps2+"_"]
        sim = scc2 in Gtrans[scc1] or scc1 in Gtrans[scc2]
        sep = scc2_ in Gtrans[scc1] or scc1_ in Gtrans[scc2]
        if sim or sep:
            a,b = find_natural_endpoints(Grel,classes,ps1,ps2,scc1,scc2)
            rel_to_edge[relation_id(a,b)](a,b)

    urls = {ps: ps+".html" for ps in proofsystems}
    longnames = {ps: proofsystems[ps]['unicodename'] for ps in proofsystems}
    nx.set_node_attributes(Gclean, urls, "URL")
    nx.set_node_attributes(Gclean, longnames, "tooltip")

    H = to_agraph(Gclean)
    H.graph_attr["forcelabels"]=True
    H.graph_attr["newrank"]=True
    H.graph_attr["splines"]="spline"
    H.graph_attr["ranksep"]=0.8

    H0 = H.add_subgraph(notpolybounded,name='cluster_notpolybounded',label="Not poly bounded",bgcolor="#f7f7f7",peripheries=0)
    #H1 = H.add_subgraph(nolowerbounds,name='cluster_nolowerbounds',label="Lower bounds not registered",peripheries=0)

# begin hirsch bugfix 2: making a subgraph for the whole SCC, not just two nodes
    for _,comp in classes.items():
        if len(comp)<2 or comp[0][-1]=='_':
            continue
        clusterparent = H0 if comp[0] in notpolybounded else H
        clusterparent.add_subgraph(comp,rank='same',name='cluster_'+comp[0],label="",peripheries=0)
# end hirsch bugfix 2

    H.graph_attr["label"]="Proof Complexity Zoo - https://proofcomplexityzoo.gitlab.io/zoo/"
    H.graph_attr["labelloc"]="b"
    H.graph_attr["labeljust"]="r"

    H.write("zoo-clean.dot")
    os.system(f"dot -T pdf zoo-clean.dot -o build/{CONFIG_OUTPUT}zoo-clean.pdf")
    os.system(f"dot -T svg zoo-clean.dot -o build/{CONFIG_OUTPUT}zoo-clean.svg")

if __name__ == '__main__':
    os.makedirs("cache", exist_ok=True)
    os.makedirs("build", exist_ok=True)
    os.makedirs("build/qpsep", exist_ok=True)
    raw_plot()
    clean_plot()
    make_pages()
    CONFIG_QP_SIM=False
    CONFIG_OUTPUT="qpsep/"
    clean_plot()
    make_pages()
